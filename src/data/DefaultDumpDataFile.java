/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javafx.util.Pair;

/**
 * represents a file from the default dump from database as it was provided the
 * data is in the 12th and 15th column - indexed from 0
 *
 * @author martin
 */
public class DefaultDumpDataFile extends DataFile {

    /**
     * instantiates default dump data file
     * @param list splitted data tokens
     * @param filename file name of original file
     * @throws Exception if there is an error on parsing
     */
    public DefaultDumpDataFile(List<String[]> list, String filename) throws Exception {
        super(filename);
        this.rawData = list;
        this.testingData = new ArrayList<>();
        this.trainingData = new ArrayList<>();

        //error tracking
        StringBuilder err = new StringBuilder();
        int l = 0;

        for (Iterator<String[]> it = list.iterator(); it.hasNext();) {
            String[] line = it.next();
            String[] training = split(line[12]);
            testingData.add(training);
            if (line[15].equals("\\N")) {
                trainingData.add(new Pair<>(training, 0L));
            } else {
                try {
                    trainingData.add(new Pair<>(training, Long.parseLong(line[15])));
                } catch (NumberFormatException e) {
                    err.append(String.format("Error on line %d : %s\n", l, line[15]));
                }
            }

            l++;
        }

        if (err.length() != 0) {
            throw new Exception("Error while converting file of type default dump\n taking long from column 15\n" + err.toString());
        }
    }
}
