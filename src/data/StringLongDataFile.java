package data;



import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author martin
 */
public class StringLongDataFile extends DataFile {
    
    /**
     * instantiates string-long data file
     * @param list splitted data tokens
     * @param filename file name of original file
     * @throws Exception if there is an error on parsing
     */
    public StringLongDataFile(List<String[]> list, String filename) throws Exception {
        super(filename);
        this.testingData = new ArrayList<>();
        this.trainingData = new ArrayList<>();
        this.rawData = list;
        
        
        StringBuilder err = new StringBuilder();
        int l = 0;
        for(Iterator<String[]> it = list.iterator(); it.hasNext();) {
            String[] line = it.next();
            String[] training = split(line[0]);
            testingData.add(training);
            try {
            trainingData.add(new Pair<>(training, Long.parseLong(line[1])));
            } catch (NumberFormatException e) {
                err.append(String.format("Error on line %d : %s\n", l, line[1]));
            }
            l++;
        }
        
        
        if(err.length() != 0) {
            throw new Exception("Error while parsing file of type String/Long\n" + err.toString());
        }
        
        
                    
              
    }

    
    
    
    
    

    
    
    

    
    
    


    

    
    
    
    
    
}
