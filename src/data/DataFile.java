/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import odatapredictor.ParseParameters;

/**
 *
 * @author martin
 */
public abstract class DataFile implements IDataFile {

    protected final String filename;


    protected List<Pair<String[], Long>> trainingData;

    protected List<String[]> testingData;

    protected List<Long> results;

    protected List<String[]> rawData;

    public DataFile(String filename) {
        this.filename = filename;
    }

    

    @Override
    public void save() {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(filename.substring(0, filename.lastIndexOf('.')) +"_"+ ParseParameters.getMethod() +"_result.csv"));

            int line = 0;
            for (String[] rd : rawData) {
                String sep = "";
                for (String s : rd) {
                    bw.write(sep + s);
                    sep = "|";
                }
                if (results != null) {
                    bw.write(sep + results.get(line));
                }

                bw.write("\n");
                line++;
            }

        } catch (IOException ex) {
            Logger.getLogger(StringDataFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(StringDataFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    @Override
    public void decorateWithResults(List<Long> l) {
        results = l;
    }

    @Override
    public List<Pair<String[], Long>> getTrainingData() {
        return trainingData;
    }

    @Override
    public List<String[]> getTestingData() {
        return testingData;
    }
    
    protected String[] split(String s) {
        return s.split("([\\s\\.:,-])+|(\\\\N)+");
    }

}
