/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.List;
import javafx.util.Pair;

/**
 * Interface prescribing a file that can be used in the prediction
 * @author martin
 */
public interface IDataFile {
    
    public List<Pair<String[], Long>> getTrainingData();
    
    public List<String[]>  getTestingData();
    
    public void save();
    
    public void decorateWithResults(List<Long> l);
    
}
