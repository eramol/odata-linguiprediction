/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * factory class that parses files and creates appropriate data files
 * @author martin
 */
public class DataFactory {

    int nline;
    int colsInLine;

    String errors;

    /**
     * file processing method
     * @param filename name of the file to be processed
     * @return appropriate descendant of the DataFile class matching the data in the file
     * @throws FileNotFoundException if the file is not found
     * @throws Exception if there is some error in format
     */
    public DataFile createDataFile(String filename) throws FileNotFoundException, Exception {
        reset();      
        List<String> fileContents = readFile(filename);
        List< String[]> csvContents = processFileData(fileContents);
        if (csvContents == null) {
            throw new Exception("Error while parsing:\n" + errors);
        }
        FileType type = guessType(csvContents.get(0));
        switch (type) {
            case DEFAULT_DUMP:
                return new DefaultDumpDataFile(csvContents, filename);
            case STRING:
                return new StringDataFile(csvContents, filename);
            case STRINGLONG:
                return new StringLongDataFile(csvContents, filename);                    
        }

        throw new Exception("File type not recognized");
    }

    /**
     * transforms the line, checks for line length inconsistency
     *
     * @param line line of text to be transformed to csv
     * @return
     * @throws Exception
     */
    protected String[] processLine(String line) {
        String[] s = line.split("(?<!\\\\)\\|");

        if (colsInLine < 0) {
            colsInLine = s.length;
        }
        if (colsInLine != s.length) {
            System.out.println(Arrays.toString(s));
            errors = errors.concat(String.format("Problem at line %d: Expected %d, received %d\n", nline, colsInLine, s.length));
        }
        nline++;
        return s;
    }

    /**
     * plain reset
     */
    protected void reset() {
        nline = 1;
        colsInLine = -1;
        errors = "";
    }

    /**
     * tries to guess the type of the file
     *
     * @param s
     * @return
     */
    protected data.FileType guessType(String[] s) {
        System.out.println(Arrays.toString(s));

        try {
            if (s.length == 1) {
                return data.FileType.STRING;
            }

            if (s.length == 2) {
                long parseLong = Long.parseLong(s[1]);
                return data.FileType.STRINGLONG;
            }

            if (s.length == 23) {
                long parseLong = Long.parseLong(s[15]);                
                return data.FileType.DEFAULT_DUMP;
            }

        } catch (NumberFormatException e) {

        }

        return null;
    }

    /**
     * reads all lines of a file, exception if fail
     *
     * @param filename
     * @return
     * @throws java.io.IOException
     */
    protected List<String> readFile(String filename) throws IOException {
        return Files.readAllLines(Paths.get(filename));
    }

    protected List<String[]> processFileData(List<String> l) {
        List<String[]> plines = new ArrayList<>();
        l.forEach(line -> plines.add(processLine(line)));
        return errors.length() == 0 ? plines : null;
    }
}
