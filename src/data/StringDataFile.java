/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author martin
 */
public class StringDataFile extends DataFile  {

    /**
     * instantiates default string data
     * @param list splitted data tokens
     * @param filename file name of original file
     */
    public StringDataFile(List<String []> list, String filename) {
        super(filename);
        
        this.testingData = new ArrayList<>();
        this.rawData = list;
        list.forEach((line) -> testingData.add(split(line[0])));                
    }
    
    
    
  
    
}
