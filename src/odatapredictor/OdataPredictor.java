/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package odatapredictor;

import data.DataFactory;
import data.DataFile;
import logic.bayes.DefaultFilter;
import logic.bayes.ILexiconFilter;
import logic.bayes.ISmoothing;
import logic.bayes.LaplaceSmoothing;
import logic.bayes.PredictorBayes;
import logic.tfidf.PredictorTFIDF;

/**
 *
 * @author martin
 */
public class OdataPredictor {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
        System.out.println("b");
        ParseParameters.parse(args);
        if (ParseParameters.isError() == true) {
            return;
        }

        
        //data processing, can throw exception
        DataFactory dfac = new DataFactory();
        DataFile training = dfac.createDataFile(ParseParameters.getTraining());
        DataFile testing = dfac.createDataFile(ParseParameters.getTesting());

        String method = ParseParameters.getMethod();

        if (method.equals("bayes")) {
            ILexiconFilter lexfilter = new DefaultFilter();
            ISmoothing smoothing = new LaplaceSmoothing();
            

            PredictorBayes p = new PredictorBayes(training, testing, smoothing, lexfilter);
            p.prepare();
            p.classify();
            p.save();
        }

        if (method.equals("tfidf")) {
            PredictorTFIDF p = new PredictorTFIDF(training, testing);
            p.prepare();
            p.classify();
            p.save();
        }
    }

}
