/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package odatapredictor;

import javafx.util.Pair;

/**
 *
 * @author martin
 */
public class ParseParameters {

    static String training;
    static String testing;
    static String method = "tfidf";
    static boolean error = false;

    public static void parse(String[] args) {
        if (args.length < 4
                || !(args[0].equals("--training") || args[0].equals("--testing"))
                || !(args[2].equals("--training") || args[2].equals("--testing"))
                || (args[0].equals(args[2]))) {
            System.out.println("Usage:\n"
                    + "./program --training trainingfile.csv --testing --testingfile.csv [tfidf | bayes]");
            error = true;
        }

        if (args[0].equals("--training")) {
            training = args[1];
            testing = args[3];
        }

        if (args[0].equals("--testing")) {
            training = args[3];
            testing = args[1];
        }

        if (args.length > 4) {
            if (args[4].equals("tfidf")) {
                method = "tfidf";
            }
            if (args[4].equals("bayes")) {
                method = "bayes";
            }
            if (args[4].equals("dummy")) {
                method = "dummy";
            }
        }

    }

    public static String getTesting() {
        return testing;
    }

    public static String getTraining() {
        return training;
    }

    public static boolean isError() {
        return error;
    }

    public static String getMethod() {
        return method;
    }

}
