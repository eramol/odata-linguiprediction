/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.bayes;

import data.IDataFile;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * @author martin
 */
public class Lexicon {

    Map<String, Long> lmap;
    ILexiconFilter lf;

    public Lexicon() {
        lmap = new HashMap<>();
    }

    /**
     * initialises lexicon from datafile and applies the filter
     * @param df data file
     * @param lf lexicon filter
     */
    public Lexicon(IDataFile df, ILexiconFilter lf) {
        this();
        initialise(df);
        this.lf = lf;
        filter();
    }

    /**
     * takes a data file and computes frequencies of words in it
     * @param df data file
     */
    public final void initialise(IDataFile df) {
        List<String[]> tdata = df.getTestingData();

        tdata
                .stream()
                .forEach(ws -> {
                    Arrays.asList(ws)
                    .stream()
                    .forEach(w -> lmap.put(w, lmap.getOrDefault(w, 0L) + 1));                    
                });
    }
    
    /**
     * applies the lexicon filter
     */
    public final void filter() {
        lf.filter(lmap);    
    }
    
    /**
     * creates a map with all know words with zero counts
     * @return 
     */
    public Map<String, Double> getZeroLexMap() {
        return lmap.keySet()
                .stream()
                .collect(Collectors.toMap(Function.identity(), k -> 0.0));
    }
    
    

}
