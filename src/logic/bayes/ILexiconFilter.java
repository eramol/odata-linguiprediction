/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.bayes;

import java.util.Map;

/**
 *
 * @author martin
 */
public interface ILexiconFilter {
    
    public void filter(Map<String, Long> map);
    
}
