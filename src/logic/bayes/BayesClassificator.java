/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.bayes;

import data.IDataFile;
import java.util.Arrays;
import java.util.Map;

/**
 *
 * @author martin
 */
public class BayesClassificator {

    protected Map<String, Double> lexmap;
    protected Long key;
    protected ISmoothing smoother;

    public BayesClassificator(Map<String, Double> lexmap, Long key, ISmoothing smoother) {
        this.lexmap = lexmap;
        this.key = key;
        this.smoother = smoother;
    }

    public void train(IDataFile df) {
        df.getTrainingData()
                .stream()
                .filter(p -> p.getValue().compareTo(key) == 0)
                .map(p -> p.getKey())
                .flatMap(ws -> Arrays.asList(ws).stream())
                .forEach(w -> {
                    if (lexmap.containsKey(w)) {
                        lexmap.put(w, lexmap.get(w) + 1);
                    } else {
                        lexmap.put("UNKNOWN", lexmap.get("UNKNOWN") + 1);
                    }
                });        
        smooth();

    }

    protected void smooth() {
        smoother.smooth(lexmap);
//        for(int i = 0; i < 10; i++)
//            System.out.println(lexmap.get((String)lexmap.keySet().toArray()[i]));
//        System.exit(0);
        
    }

    public double estimate(String[] ss) {
        double unk = lexmap.get("UNKNOWN");        
//        for(String s : ss)
//            System.out.println(String.format("%s %b", s, lexmap.containsKey(s)));
        
        return Arrays.asList(ss)
                .stream()
                .map(token -> lexmap.getOrDefault(token, unk))
                .map(val -> -Math.log(val))
                .mapToDouble(Double::doubleValue)
                .sum();        
    }

    public Long getKey() {
        return key;
    }
    
    

}
