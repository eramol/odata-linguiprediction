/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.bayes;

import java.util.Map;

/**
 *
 * @author martin
 */
public interface ISmoothing {
    
    
    public void smooth(Map<String,Double> m);
    
}
