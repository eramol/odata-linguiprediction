/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.bayes;

import data.IDataFile;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author martin
 */
public class PredictorBayes {

    protected IDataFile training;
    protected IDataFile testing;
    protected ISmoothing smoothing;
    protected ILexiconFilter filter;

    protected Lexicon lexicon;

    protected Set<Long> longIds;
    protected Long[] resIds;
    protected Double[] resVals;
    
    protected static int logCounter;

//    protected List<BayesClassificator> classificators;  
    public PredictorBayes(IDataFile training, IDataFile testing, ISmoothing smoothing, ILexiconFilter filter) {
        this.training = training;
        this.testing = testing;
        this.smoothing = smoothing;
        this.filter = filter;
        lexicon = new Lexicon(training, filter);
//        classificators = new ArrayList<>();
    }

    public void prepare() {
        longIds = training.getTrainingData()
                .stream()
                .map(p -> p.getValue())
                .collect(Collectors.toSet());

        resIds = new Long[training.getTestingData().size()];
        resVals = new Double[training.getTestingData().size()];
        Arrays.fill(resVals, Double.MAX_VALUE);
        
        

//        System.out.println("" + lids.size());
//        System.exit(0);
//        lids.stream().forEach(lid -> classificators.add(
//                new BayesClassificator(lexicon.getZeroLexMap(), lid, smoothing))
//        );
//
//        System.out.println(String.format("training %d models", classificators.size()));
//        classificators.stream().forEach(c -> c.train(training));
//        System.out.println("finished");
    }

    public void classify() {
        PredictorBayes.logCounter = 0;
        
        
        System.out.println("classification");
        longIds
                .parallelStream()
                .map(p -> new BayesClassificator(lexicon.getZeroLexMap(), p, smoothing))
                .forEach(c ->
                {
                    c.train(training);
                    c.smooth();
//                    System.out.println("working...");
                    int index = 0;
                    for(String [] l : testing.getTestingData()) {
                        saveValue(index, c.getKey(), c.estimate(l));
                        index++;
                    }
                    log();
                });
        
        
        testing.decorateWithResults(Arrays.asList(resIds));
    }

    public void save() {
        testing.save();
    }
    
    protected synchronized  void saveValue(int pos, Long id, Double val) {
        
//        System.out.println(String.format("(%d %d %f)", pos, id, val));
        
        if(resVals[pos] > val) {
            resVals[pos] = val;
            resIds[pos] = id;
        }
    }
    
    protected synchronized void log() {
        System.out.println(String.format("done %d/%d", ++logCounter, longIds.size()));
    }

}
