/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.bayes;

import java.util.Map;

/**
 * basic laplace add-one smoothing
 * @author martin
 */
public class LaplaceSmoothing implements ISmoothing {

    

    @Override
    public void smooth(Map<String, Double> m) {
        double sum = m.values().stream().mapToDouble(Double::doubleValue).sum() + m.size();
        
        m.keySet()
                .stream()
                .forEach(k -> m.put(k, (m.get(k) + 1)/sum));
    }
    
    
    
}
