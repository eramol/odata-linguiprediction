/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.bayes;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author martin
 */
public class DefaultFilter implements ILexiconFilter {

    /**
     * extracts 5 most common words extracts words of length < 2 extracts words
     * which appeared only once adds the UNKNOWN token
     *
     * @param map input/output map
     */
    @Override
    public void filter(Map<String, Long> map) {
        System.out.println("Filtering");
        List<String> keys = map.keySet()
                .stream()
                .filter(k -> map.get(k) > 1)
                .filter(k -> k.length() > 1)
                .sorted((w1, w2) -> map.get(w2).compareTo(map.get(w1)))
                .collect(Collectors.toList());

        keys = keys.subList(5, keys.size());
        Set<String> keyset = new HashSet<>(keys);

        //unknown hack
        keyset.add("UNKNOWN");
        map.put("UNKNOWN", 0L);

        Set<String> keysb = map.keySet()
                .stream()
                .filter(k -> !keyset.contains(k))
                .collect(Collectors.toSet());
        
        keysb.forEach(k -> map.remove(k));
        
    }

}
