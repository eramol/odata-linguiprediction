/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.tfidf;

import data.IDataFile;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

/**
 * IDF score storing class
 * @author martin
 */
public class IDFsupport {

    static Map<String, Double> idfmap;

    /**
     * computes an IDF score from the training database
     * @param training training datafile
     */
    public static void initialise(IDataFile training) {
        idfmap = new HashMap<>();

        Set<String> tempSet = new HashSet();
        training.getTestingData()
                .stream()
                .forEach(s -> {
                    tempSet.clear();
                    tempSet.addAll(Arrays.asList(s));
                    tempSet.stream().forEach(w -> idfmap.put(w, idfmap.getOrDefault(w, 0.0) + 1));
                });
        
        
        //transforming word document occurence (WDO) into inverse idf
        // log(1 + WDO/Ndoc)
        int ndocs = training.getTestingData().size();
        Function<Double, Double> ffw = wc -> Math.log(1 + wc / ndocs);

        idfmap.keySet().stream().forEach(w -> idfmap.put(w, ffw.apply(idfmap.get(w))));
    }

    public static synchronized Map<String, Double> getIdfmap() {
        return idfmap;
    }
    
    
    

}
