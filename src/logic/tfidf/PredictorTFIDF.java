/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.tfidf;

import data.IDataFile;
import java.util.Arrays;

/**
 * main predictor class of TF-IDF prediction
 * @author martin
 */
public class PredictorTFIDF {

    protected IDataFile training;
    protected IDataFile testing;

    //result ids and values
    protected Long[] resIds;
    protected Double[] resVals;
    
    protected static int logCounter;

    public PredictorTFIDF(IDataFile training, IDataFile testing) {
        this.training = training;
        this.testing = testing;
    }
    
    /**
     * initialisation of idf scores, if not done before
     * allocation of resIds, resVals arrays
     */
    public void prepare() {
        if (IDFsupport.getIdfmap() == null) {
            IDFsupport.initialise(training);
        }

        resIds = new Long[training.getTestingData().size()];
        resVals = new Double[testing.getTestingData().size()];
        Arrays.fill(resVals, Double.MAX_VALUE);
    }

    /**
     * Classification. For each training data, computes in parallel the tf-idf
     * distance to all testing data and saves it accordingly.
     * Decorates the testing data with results in the end.
     */
    public void classify() {
        training.getTrainingData()
                .parallelStream()
                .forEach(pair -> {
                    TSIDFclassifier tsidfc = new TSIDFclassifier();
                    int pos = 0;
                    for (String[] t : testing.getTestingData()) {
                        saveValue(pos,
                                pair.getValue(),
                                tsidfc.TFIDFcosdistance(pair.getKey(), t));
                        pos++;
                    }
                    
                    log();
                });
        
        testing.decorateWithResults(Arrays.asList(resIds));
    }

    /**
     * dump decorated file
     */
    public void save() {
        testing.save();
    }

    /**
     * synchronized method to save a computed value
     * @param pos position number of testing data
     * @param id id of the contestant
     * @param val distance
     */
    protected synchronized void saveValue(int pos, Long id, Double val) {

//        System.out.println(String.format("(%d %d %f)", pos, id, val));
        if (resVals[pos] > val) {
            resVals[pos] = val;
            resIds[pos] = id;
        }
    }
    
    
    protected synchronized void log() {
            System.out.println(String.format("done %d/%d", ++logCounter, training.getTestingData().size()));
    }


}
