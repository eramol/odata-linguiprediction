/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.tfidf;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;

/**
 * single classifier class
 * @author martin
 */
public class TSIDFclassifier {

    Map<String, Integer> lf;
    Map<String, Integer> ls;
    Map<String, Double> idfmap;

    public TSIDFclassifier() {
        lf = new HashMap<>();
        ls = new HashMap<>();
        idfmap = IDFsupport.getIdfmap();
    }

    /**
     * absolute distance between two documents
     *
     * @param tr array of tokens of document 1
     * @param ts array of tokens of document 2
     * @return sum of absolute distances between documents' tf-idf scores
     */
    public Double TFIDFdistance(String[] tr, String[] ts) {
        lf.clear();
        ls.clear();

        Arrays.asList(tr).stream()
                .forEach(w -> lf.put(w, lf.getOrDefault(w, 0) + 1));
        Arrays.asList(ts).stream()
                .forEach(w -> ls.put(w, ls.getOrDefault(w, 0) + 1));

        double dist = 0.0;

        for (String w : lf.keySet()) {
            if (ls.get(w) == null) {
                //only training set 
                dist += 1.0 / lf.size() * lf.get(w);
            } else {
                //intersection
                double a = 1.0 / lf.size() * lf.get(w) - 1.0 / ls.size() * ls.get(w);
                double b = 1.0 / ls.size() * ls.get(w) - 1.0 / lf.size() * lf.get(w);
                dist += a > b ? a : b;
            }
        }

        for (String w : ls.keySet()) {
            if (lf.get(w) == null) {
                //only testing set (getOrDefault - may have not been present in training)
                dist += 1.0 / ls.size() * ls.get(w) * idfmap.getOrDefault(w, 0.0);
            }
        }

        return dist;
    }

    /**
     * inverted similarity distance between two documents
     *
     * @param tr array of tokens of document 1
     * @param ts array of tokens of document 2
     * @return cosine distance between documents
     */
    public double TFIDFcosdistance(String[] tr, String[] ts) {
        lf.clear();
        ls.clear();

        Arrays.asList(tr).stream()
                .forEach(w -> lf.put(w, lf.getOrDefault(w, 0) + 1));
        Arrays.asList(ts).stream()
                .forEach(w -> ls.put(w, ls.getOrDefault(w, 0) + 1));

        int maxlf = lf.values().stream().reduce(0, (a, b) -> Integer.max(a, b));
        int maxls = ls.values().stream().reduce(0, (a, b) -> Integer.max(a, b));

        double dist = 0.0;

        
        
        //intersection
        for(String w : lf.keySet()) {
            if(!ls.containsKey(w))
                continue;
                        
            dist += (0.5 + 0.5 * lf.get(w) / maxlf) * (0.5 + 0.5 * ls.get(w) / maxls)
                        * idfmap.getOrDefault(w, 0.0) * idfmap.getOrDefault(w, 0.0);            
        }
        
                

        dist /= (Math.sqrt(ls.keySet().stream().map(w -> idfmap.getOrDefault(w, 0.0) * (0.5 + 0.5 * ls.get(w) / maxls)).mapToDouble(v -> v * v).sum())
                * Math.sqrt(lf.keySet().stream().map(w -> idfmap.getOrDefault(w, 0.0) * (0.5 + 0.5 * lf.get(w) / maxlf)).mapToDouble(v -> v * v).sum()));

        return -dist;
    }

}
